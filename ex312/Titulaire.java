package ex312;

public class Titulaire {
	private String nom;
	private TableauCompte mesComptes = new TableauCompte(10);
	
	Titulaire(String n){
		nom = n;
	}
	
	public String getName() {
		return nom;
	}
	
	public TableauCompte getComptes() {
		return mesComptes;
	}
	
	public void ajouter(Compte c) throws NonInitialise {
		mesComptes.ajouter(c);
	}
	
	
	public String toString() {
		return String.format("%s%n%s%n%s", getName(), "Comptes", mesComptes.toString());
	}
	
	
}
