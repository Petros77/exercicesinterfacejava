package ex312;

public class Compte {
	static int code;
	int solde = 0;
	int numero;
	public Compte() {
		this.numero = this.code;
		this.solde = 0;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public int getSolde() {
		return solde;
	}
	
	void depot(int n){
		solde = solde + n;
	}
	
	void retrait(int n){
		solde = solde - n;
	}
	
	void afficher(){
		System.out.print("solde du compte numero " + getNumero() + ": ");
		System.out.print(getSolde());
	}
	
	
	@Override
	public String toString()  {
		return String.format("%s : %d   %s : %d", "numero", getNumero(), "solde", getSolde());
	}
	
}

class NonInitialise extends Exception{}

