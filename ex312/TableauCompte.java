package ex312;

public class TableauCompte {
		Compte[] tab;
		int longueur;
		
		TableauCompte(int n){
			tab = new Compte[n];
		}
		
		void ajouter(Compte c) throws NonInitialise{
 			if (c == null){
				throw new NonInitialise();
			}
			if (longueur < tab.length){
				tab[longueur]=c;
				longueur++;
			}
		
		}
		
		public Compte getCompte() {
			return tab[longueur];
		}
		
		@Override
		public String toString() {
			String comptes = "";
			for (int i = 0; i < tab.length; i++) {
				comptes = comptes +  tab[i].toString();
				
			}
			return comptes;
			
		}


}
