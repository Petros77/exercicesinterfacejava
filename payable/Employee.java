


public abstract class Employee {
	private final String firstName;
	private final String lastName;
	private final String socialSecuNum;
	
	public Employee(String firstName, String lastName, String socialSecuNum) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecuNum = socialSecuNum;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getSocialSecuNum() {
		return socialSecuNum;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s%nsocial security number : %s", getFirstName(), getLastName(), getSocialSecuNum());
	}
	
	public abstract double earnings();
}
