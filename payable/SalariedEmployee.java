public class SalariedEmployee extends Employee implements Payable {
	private double baseSalary;
	
	public SalariedEmployee(String firstName, String lastName, String socialSecuNum, double baseSalary) {
		super(firstName, lastName, socialSecuNum);
		if (baseSalary < 0) {
			throw new IllegalArgumentException("Base sallary must be bigger than 0");
		} 
		 this.baseSalary = baseSalary;
	}
	
	public void setBaseSalary(double baseSalary) {
		if (baseSalary < 0) {
			throw new IllegalArgumentException("Base Salary must be bigger than 0");
		}
		
		this.baseSalary = baseSalary;
	}
	
	public double getBaseSalary() {
		return baseSalary;
	}
	
	@Override
	public double earnings() {
		return getBaseSalary();
	}
	@Override
	public double getPaymentAmount() {
		return earnings();
	}

	@Override
	public String toString() {
		return String.format("%s%n%s%n %s : �%f", "Salaried employe" , super.toString(), "salary" , earnings());
	}
}
